package com.sattar.shiftprovider.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sattar.shiftprovider.Activity.MainActivity;
import com.sattar.shiftprovider.R;

/**
 * Created by moataz on 3/10/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

//    private InternetConnection internetConnection;
//    private TokenRequest tokenRequest;
//    private UserData userData;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
//        sendRegistrationToServer(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.d(getString(R.string.TAG), "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            // Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String title = remoteMessage.getNotification().getTitle();
            String messageBody = remoteMessage.getNotification().getBody();
            sendNotification(title, messageBody);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void handleNow() {
    }

    private void scheduleJob() {
    }

    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        assert notificationManager != null;
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

//    private void sendRegistrationToServer(final String refreshedToken) {
//        try {
//            internetConnection = new InternetConnection();
//
//            if (internetConnection.isNetworkAvailable(getApplicationContext())) {
//                Ion.with(getApplicationContext())
//                        .load(getString(R.string.Resource_Address) + getString(R.string.UpdateToken_Address))
//                        .addHeader("Authorization", "Bearer " + userData.Get_UserToken(getApplicationContext()))
//                        .setBodyParameter("firebaseToken", refreshedToken.trim())
//                        .asJsonObject()
//                        .setCallback(new FutureCallback<JsonObject>() {
//                            @Override
//                            public void onCompleted(Exception e, JsonObject result) {
//                                if (result != null && !Objects.equals(result, "[]")) {
//
//                                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
//                                    tokenRequest = gson.fromJson(result.getAsJsonObject("response"), TokenRequest.class);
//                                    if (Objects.equals(tokenRequest.getStatus(), "true")) {
//
//                                    } else {
////
////                                        Snackbar snackbar = Snackbar
////                                                .make(mScrollViewContainer, "there was an error in saving your firebase id", Snackbar.LENGTH_LONG);
////                                        snackbar.show();
//                                    }
//                                }
//                            }
//                        });
//
//            } else {
//                userData.Clear_UserData(getApplicationContext());
//            }
//        } catch (Exception e) {
////            runOnUiThread(new Runnable() {
////                @Override
////                public void run() {
////                    mSignInButton.setProgress(-1);
////                }
////            });
////            Snackbar snackbar = Snackbar
////                    .make(mScrollViewContainer, "there is an Error try again later.", Snackbar.LENGTH_LONG);
////            snackbar.show();
//            Log.e(getString(R.string.TAG), "MyFirebaseMessagingService Exception = " + String.valueOf(e));
//
//        }
//    }
}
