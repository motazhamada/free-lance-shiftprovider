package com.sattar.shiftprovider.Classes;

import com.google.gson.annotations.SerializedName;

public class TokenResponseUser extends TokenResponse {
    @SerializedName("data")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
