package com.sattar.shiftprovider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pkmmte.view.CircularImageView;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.SavePrefs;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CancelOrderActivity extends AppCompatActivity {

    Button send_button_co;
    CircularImageView client_item_imageView_co;
    TextView order_price_co, clientName_item_textView_co, clientPhone_item_textView_co, clientServiceType_item_textView_co, clientTime_item_textView_co;
    RadioGroup reason_radioGroup_co;
    EditText feedback_editText_co;

    ProgressBar progressBar;

    AlertDialog.Builder builder;

    String reason = "1";
    private SavePrefs savePrefs;
    private JobItem mJobItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_order);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        //noinspection unchecked
        savePrefs = new SavePrefs(this, JobItem.class);
        mJobItem = (JobItem) savePrefs.load();


        send_button_co = findViewById(R.id.send_button_co);

        client_item_imageView_co = findViewById(R.id.client_item_imageView_co);
        order_price_co = findViewById(R.id.order_price_co);
        clientName_item_textView_co = findViewById(R.id.clientName_item_textView_co);
        clientPhone_item_textView_co = findViewById(R.id.clientPhone_item_textView_co);
        clientServiceType_item_textView_co = findViewById(R.id.clientServiceType_item_textView_co);
        clientTime_item_textView_co = findViewById(R.id.clientTime_item_textView_co);

        reason_radioGroup_co = findViewById(R.id.reason_radioGroup_co);
        if (reason_radioGroup_co.getCheckedRadioButtonId() == R.id.reason_1_radioButton_co) {
            reason = "1";
        } else if (reason_radioGroup_co.getCheckedRadioButtonId() == R.id.reason_2_radioButton_co) {
            reason = "2";
        } else {
            reason = "3";
        }


        feedback_editText_co = findViewById(R.id.feedback_editText_co);

        progressBar = findViewById(R.id.progressBar_co);

        builder = new AlertDialog.Builder(CancelOrderActivity.this);

        send_button_co.setOnClickListener(v -> {
            builder.setMessage("Are you sure you want to Cancel this order ?")
                    .setTitle("Cancel");
            builder.setPositiveButton("Yes", (dialog, id) -> {
                CancelJob();
                dialog.dismiss();
            });
            builder.setNegativeButton("No", (dialog, id) -> {
                dialog.cancel();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        loadData();
    }

    private void loadData() {
        order_price_co.setText(String.format("%s SAR", mJobItem.getPrice()));
        clientName_item_textView_co.setText(mJobItem.getClient_name());
        clientPhone_item_textView_co.setText(mJobItem.getClient_mobile());
        clientServiceType_item_textView_co.setText(String.format("Service : %s", mJobItem.getService_name()));
        clientTime_item_textView_co.setText(String.format("Time : %s", mJobItem.getTime_from()));

    }

    void showPB(boolean status) {
        if (status) {
            progressBar.setVisibility(View.VISIBLE);
            send_button_co.setVisibility(View.GONE);

        } else {
            progressBar.setVisibility(View.GONE);
            send_button_co.setVisibility(View.VISIBLE);

        }
    }

    void CancelJob() {
        // data to populate the RecyclerView with
        try {
            String comment = feedback_editText_co.getText().toString();
            if (Objects.equals(comment.trim(), "")){
                comment="No Comment";
            }
            showPB(true);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            String finalComment = comment;
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Make_Cancel_Order_Address),
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                Log.d(getString(R.string.TAG), "response : " + response);
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                if (Objects.equals(jsonObject.getBoolean("status"), true)) {
                                    Intent intent = new Intent(CancelOrderActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                    savePrefs.clear();
                                }
                                showPB(false);
                            } catch (Exception error) {
                                showPB(false);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            showPB(false);
                            Log.e(getString(R.string.TAG), "OrderActivity 2");
                        }
                    }, error -> {
                Log.e(getString(R.string.TAG), "OrderActivity 1");
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("order_id", mJobItem.getId());
                    params.put("reason", reason);
                    params.put("comment", finalComment);
                    params.put("lang", getString(R.string.lang));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "OrderActivity 3: " + e.toString());
        }
    }


}
