package com.sattar.shiftprovider.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;
import com.pkmmte.view.CircularImageView;
import com.sattar.shiftprovider.Classes.DataParser;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.SavePrefs;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int DEFAULT_ZOOM = 15;

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    @BindView(R.id.action_button_bs)
    Button action_button_bs;
    @BindView(R.id.directions_button_bs)
    Button directions_button_bs;
    @BindView(R.id.client_item_imageView_bs)
    CircularImageView client_item_imageView_bs;
    @BindView(R.id.order_price_bs)
    TextView order_price_bs;
    @BindView(R.id.clientName_item_textView_bs)
    TextView clientName_item_textView_bs;
    @BindView(R.id.clientPhone_item_textView_bs)
    TextView clientPhone_item_textView_bs;
    @BindView(R.id.clientServiceType_item_textView_bs)
    TextView clientServiceType_item_textView_bs;
    @BindView(R.id.clientTime_item_textView_bs)
    TextView clientTime_item_textView_bs;
    @BindView(R.id.progressBar_bs)
    ProgressBar progressBar;
    //    String CurrentJobID = null;
    AlertDialog.Builder builder;
    //    Bundle bundle;
    private Location mDefaultLocation;
    private boolean isDeviceLocationSetted = false;
    private SupportMapFragment mapView;
    private GoogleMap mGoogleMap;
    private CameraPosition mCameraPosition;
    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private Location mLastKnownLocation;
    private BitmapDescriptor icon;
    private double mDestLat, mDestLng;
    private SavePrefs savePrefs;
    private JobItem mJobItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        //noinspection unchecked
        savePrefs = new SavePrefs(this, JobItem.class);
        mJobItem = (JobItem) savePrefs.load();
        setDestinationLocation();

        mDefaultLocation = new Location("Wrong Location");
        mDefaultLocation.setLatitude(30.054799);
        mDefaultLocation.setLongitude(31.203546);

        icon = BitmapDescriptorFactory.fromResource(R.drawable.car_logo);

        Check_GPS();

//        CurrentJobID = bundle.getString("Order_ID");

        builder = new AlertDialog.Builder(OrderActivity.this);
        loadData();

        ClickListeners();
    }

    private void setDestinationLocation() {
        try {
            mDestLat = Double.parseDouble(mJobItem.getClient_latitude());
            mDestLng = Double.parseDouble(mJobItem.getClient_longitude());
            if (Objects.equals(mDestLat, null) || Objects.equals(mDestLng, null)) {
                mDestLat = 30.054799;
                mDestLng = 31.203546;
            }
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
            mDestLat = 30.054799;
            mDestLng = 31.203546;
        }
    }

    private void ClickListeners() {
        action_button_bs.setOnClickListener(v -> {
            String titleStatus, messageStatus;
            switch (mJobItem.getOrder_status_local()) {
                case "on_road":
                    //make it for ARRIVAL
                    titleStatus = getString(R.string.alertDialod_title_arrival);
                    messageStatus = getString(R.string.alertDialod_message_arrival);
                    break;
                case "arrival":
                    //make it for START WORK
                    titleStatus = getString(R.string.alertDialod_title_start_work);
                    messageStatus = getString(R.string.alertDialod_message_start_work);
                    break;
                case "start_work":
                    //make it for FINISHED
                    titleStatus = getString(R.string.alertDialod_title_finished);
                    messageStatus = getString(R.string.alertDialod_message_finished);
                    break;
                default:
                    titleStatus = getString(R.string.alertDialod_title_on_road);
                    messageStatus = getString(R.string.alertDialod_message_on_road);
                    break;
            }
            builder.setMessage(messageStatus)
                    .setTitle(titleStatus);
            builder.setPositiveButton(R.string.alertDialog_yes, (dialog, id) -> {
                ActionJob();
                dialog.dismiss();
            });
            builder.setNegativeButton(R.string.alertDialog_no, (dialog, id) -> dialog.cancel());
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        directions_button_bs.setOnClickListener(v -> {
            try {
                if (Objects.equals(mJobItem.getOrder_status_local(), "not_started")) {
                    builder.setMessage(R.string.alertDialog_message_on_road_order)
                            .setTitle(R.string.alertDialog_title_on_road_order);
                    builder.setPositiveButton(R.string.alertDialog_yes, (dialog, id) -> {
                        ActionJob();
                        dialog.dismiss();
                        OpenMapsForDirections();
                    });
                    builder.setNegativeButton(R.string.alertDialog_no, (dialog, id) -> {
                        dialog.cancel();
                        OpenMapsForDirections();
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    OpenMapsForDirections();
                }
            } catch (Exception e) {
                Log.e(getString(R.string.TAG), "OrderActivity 2: " + e.toString());
            }
        });
    }

    private void OpenMapsForDirections() {
        String url = "http://maps.google.com/maps?daddr=" + mDestLat + "," + mDestLng;
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        startActivity(intent);
    }


    private void Check_GPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            Initialize_Map();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("No", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void Initialize_Map() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mapView = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView.getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mGoogleMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mGoogleMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.cancel_order, menu);
            return true;
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "OrderActivity 1: " + e.toString());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_cancel:
                item.setChecked(true);
                builder.setMessage(R.string.alertDialog_message_cancel_order)
                        .setTitle(R.string.alertDialog_title_cancel_order);
                builder.setPositiveButton(R.string.alertDialog_yes, (dialog, id) -> {
                    Intent intent = new Intent(OrderActivity.this, CancelOrderActivity.class);
                    startActivity(intent);
                    dialog.dismiss();
                });
                builder.setNegativeButton(R.string.alertDialog_no, (dialog, id) -> dialog.cancel());
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mJobItem.getOrder_status_local().equals("not_started"))
            super.onBackPressed();
    }

    private void loadData() {
        loadButton();
        order_price_bs.setText(String.format("%s SAR", mJobItem.getPrice()));
        clientName_item_textView_bs.setText(mJobItem.getClient_name());
        clientPhone_item_textView_bs.setText(mJobItem.getClient_mobile());
        clientServiceType_item_textView_bs.setText(String.format("Service : %s", mJobItem.getService_name()));
        clientTime_item_textView_bs.setText(String.format("Time : %s", mJobItem.getTime_from()));

    }

    private void loadButton() {
        switch (mJobItem.getOrder_status_local()) {
            case "on_road":
                //make it for Arrival
                action_button_bs.setText(R.string.btn_arrival);
                action_button_bs.setBackgroundResource(R.color.colorOrange);
                break;
            case "arrival":
                //make it for START WORK
                action_button_bs.setText(R.string.btn_start_work);
                action_button_bs.setBackgroundResource(R.color.colorGreen);

                break;
            case "start_work":
                //make it for FINISHED
                action_button_bs.setText(R.string.btn_finished);
                action_button_bs.setBackgroundResource(R.color.colorRedDark);
                break;
            case "finish":
                //make it for after FINISHED
                IntentToFinishedOrderActivity();
                return;
        }
    }


    private void getDeviceLocation() {
        try {
            if (isDeviceLocationSetted)
                return;

            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if (mLocationPermissionGranted) {
                if (Objects.requireNonNull(manager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    // Get the current location of the device and set the position of the map.
                    Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                    locationResult.addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
//                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                                    new LatLng(mLastKnownLocation.getLatitude(),
//                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                            mDefaultLocation.setProvider("MyLocation");
                            mDefaultLocation.setLatitude(mLastKnownLocation.getLatitude());
                            mDefaultLocation.setLongitude(mLastKnownLocation.getLongitude());

                            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);

//                            mGoogleMap.addMarker(new MarkerOptions()
//                                    .title("My Location")
//                                    .position(
//                                            new LatLng(mLastKnownLocation.getLatitude(),
//                                                    mLastKnownLocation.getLongitude()))
//                                    .snippet("this is my location now"));
                            isDeviceLocationSetted = true;
                            DrawRoute();
                        }
                    });
                }
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Check_GPS();
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mGoogleMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mGoogleMap.setMyLocationEnabled(false);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        // Use a custom info window adapter to handle multiple lines of text in the
        // info window contents.
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            // Return null here, so that getInfoContents() is called next.
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Inflate the layouts for the info window, title and snippet.
                View infoWindow = getLayoutInflater().inflate(R.layout.custom_info_contents,
                        findViewById(R.id.map), false);

                TextView title = infoWindow.findViewById(R.id.title);
                title.setText(marker.getTitle());

                TextView snippet = infoWindow.findViewById(R.id.snippet);
                snippet.setText(marker.getSnippet());
                return infoWindow;
            }
        });
        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();
        // Get the current location of the device and set the position of the map.
        DrawMarker();

        getDeviceLocation();


//        if (!isDeviceLocationSetted)
//            return;

        //move map camera
//        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mDefaultLocation.getLatitude(), mDefaultLocation.getLongitude())));
//        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void DrawRoute() {
        Location mLocationA;
        mLocationA = new Location("DestinationLocation");
        mLocationA.setLatitude(mDestLat);
        mLocationA.setLongitude(mDestLng);

        String url = getUrl(mDefaultLocation, mLocationA);
        FetchUrl FetchUrl = new FetchUrl();
        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
    }

    private void DrawMarker() {
        try {
            // Add a marker in Sydney and move the camera
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(mJobItem.getClient_name()).icon(icon)
                    .position(new LatLng(mDestLat, mDestLng))
                    .snippet(mJobItem.getService_name()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mDestLat, mDestLng), DEFAULT_ZOOM));
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        } catch (Exception error) {
            Log.e(getString(R.string.TAG), "Error: " + error.getMessage());
        }
    }

    void showPB(boolean statu) {
        if (statu) {
            progressBar.setVisibility(View.VISIBLE);
            action_button_bs.setVisibility(View.GONE);
            directions_button_bs.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            action_button_bs.setVisibility(View.VISIBLE);
            directions_button_bs.setVisibility(View.VISIBLE);
        }
    }


    void ActionJob() {
        // data to populate the RecyclerView with
        try {
            showPB(true);
            String url, newStatus;
            switch (mJobItem.getOrder_status_local()) {
                case "on_road":
                    //make it for Arrival
                    newStatus = "arrival";
                    url = getString(R.string.Resource_Address) + getString(R.string.Make_Arrival_Order_Address);
                    break;
                case "arrival":
                    //make it for START WORK
                    newStatus = "start_work";
                    url = getString(R.string.Resource_Address) + getString(R.string.Make_Doing_Order_Address);
                    break;
                case "start_work":
                    //make it for FINISHED
                    newStatus = "finish";
                    url = getString(R.string.Resource_Address) + getString(R.string.Make_Finish_Order_Address);
                    break;
//                case "finish":
//                    //make it for after FINISHED
//                    Intent intent = new Intent(OrderActivity.this, FinishedOrderActivity.class);
//                    startActivity(intent);
//                    finish();
//                    return;
                default:
                    newStatus = "on_road";
                    url = getString(R.string.Resource_Address) + getString(R.string.Make_OnRoad_Order_Address);
                    break;
            }
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest request = new StringRequest(Request.Method.POST,
                    url,
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                Log.d(getString(R.string.TAG), "response : " + response);
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                if (Objects.equals(jsonObject.getBoolean("status"), true)) {
                                    mJobItem.setOrder_status_local(newStatus);
                                    //noinspection unchecked
                                    savePrefs.save(mJobItem);
                                    loadButton();
                                }
                                showPB(false);
                            } catch (Exception error) {
                                showPB(false);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            showPB(false);
                            Log.e(getString(R.string.TAG), "OrderActivity 2");
                        }
                    }, error -> Log.e(getString(R.string.TAG), "OrderActivity 1")) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("order_id", mJobItem.getId());
                    params.put("lang", getString(R.string.lang));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "OrderActivity 3: " + e.toString());
        }
    }

    private void IntentToFinishedOrderActivity() {
        Intent intent = new Intent(OrderActivity.this, FinishedOrderActivity.class);
        startActivity(intent);
        finish();
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private String getUrl(Location origin, Location dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLatitude() + "," + origin.getLongitude();

        // Destination of route
        String str_dest = "destination=" + dest.getLatitude() + "," + dest.getLongitude();


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data);
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0]);
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", "Executing routes");
                Log.d("ParserTask", routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(15);
                lineOptions.color(getResources().getColor(R.color.colorPrimary));

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mGoogleMap.addPolyline(lineOptions);
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
        }
    }
}