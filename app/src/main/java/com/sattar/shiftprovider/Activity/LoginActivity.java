package com.sattar.shiftprovider.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.koushikdutta.ion.Ion;
import com.sattar.shiftprovider.Classes.TokenResponseUser;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.InternetConnection;
import com.sattar.shiftprovider.utilities.UserData;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via name/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    @BindView(R.id.login_scrollView)
    ScrollView mParentLayout;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    // UI references.
    private AutoCompleteTextView mNameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private View Activity_Login;
    private TokenResponseUser tokenResponseUser;
    private UserData userData;
    //    private String deviceToken;
    private InternetConnection internetConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userData = new UserData();
        if (userData.Check_UserID(this)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        internetConnection = new InternetConnection();
        // Set up the login form.
        mNameView = findViewById(R.id.name);
        populateAutoComplete();

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        Button mNameSignInButton = findViewById(R.id.name_sign_in_button);
        mNameSignInButton.setOnClickListener(view -> attemptLogin());

        Activity_Login = findViewById(R.id.Activity_Login);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mNameView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, v -> requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS));
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid name, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String name = mNameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid name address.
        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }
//        else if (!isnameValid(name)) {
//            mNameView.setError(getString(R.string.error_invalid_name));
//            focusView = mNameView;
//            cancel = true;
//        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mAuthTask = new UserLoginTask(name, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 2;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        runOnUiThread(() -> {
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only name addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Nickname
                .CONTENT_ITEM_TYPE},

                // Show primary name addresses first. Note that there won't be
                // a primary name address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> names = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            names.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addnamesToAutoComplete(names);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addnamesToAutoComplete(List<String> nameAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, nameAddressCollection);

        mNameView.setAdapter(adapter);
    }

    void Set_UserData(String UserData) {
        SharedPreferences.Editor sharedPref = getSharedPreferences("UserData", MODE_PRIVATE).edit();
        sharedPref.putString("JsonUserData", UserData);
        sharedPref.apply();
        sharedPref.commit();
    }

    boolean Check_UserData() {
        SharedPreferences sharedPref = getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("JsonUserData", null);
        return restoredText != null;
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Nickname.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Nickname.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mName;
        private final String mPassword;

        UserLoginTask(String name, String password) {
            mName = name;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if (internetConnection.isNetworkAvailable(LoginActivity.this)) {
                    showProgress(true);
//                    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> deviceToken = instanceIdResult.getToken());
                    // Instantiate the RequestQueue.
                    Ion.with(LoginActivity.this)
                            .load(getString(R.string.Resource_Address) + getString(R.string.Login_Address))
                            .setBodyParameter("name", mName.trim())
                            .setBodyParameter("password", mPassword)
                            .setBodyParameter("lang", getString(R.string.lang))
                            .setBodyParameter("platform", "Android")
                            .asJsonObject()
                            .setCallback((e, result) -> {
                                if (result != null && !Objects.equals(result, "[]")) {
                                    try {
                                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                        tokenResponseUser = gson.fromJson(result, TokenResponseUser.class);
                                        if (Objects.equals(tokenResponseUser.getStatus(), "true")) {
                                            userData.Set_UserID(LoginActivity.this, tokenResponseUser.getUser().getId());
                                            userData.Set_UserData(LoginActivity.this, tokenResponseUser.getUser());
                                            if (userData.Check_UserID(LoginActivity.this)) {
                                                Snackbar snackbar = Snackbar
                                                        .make(Activity_Login, tokenResponseUser.getMessage(), Snackbar.LENGTH_LONG);
                                                snackbar.show();
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                mPasswordView.requestFocus();
                                                showProgress(false);
                                                Snackbar snackbar = Snackbar
                                                        .make(Activity_Login, "Couldn't save user data", Snackbar.LENGTH_LONG);
                                                snackbar.show();
                                            }
                                        } else {
                                            mPasswordView.requestFocus();
                                            showProgress(false);
                                            Snackbar snackbar = Snackbar
                                                    .make(Activity_Login, tokenResponseUser.getMessage(), Snackbar.LENGTH_LONG);
                                            snackbar.show();
                                        }
                                    } catch (Exception e1) {
                                        Log.e(getString(R.string.TAG), "Error: " + e1.getMessage());
                                        Snackbar snackbar = Snackbar
                                                .make(Activity_Login, "there is an Error try again later.", Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                        showProgress(false);
                                    }
                                } else {
                                    mPasswordView.requestFocus();
                                    Snackbar snackbar = Snackbar
                                            .make(Activity_Login, "there is an Error try again later.", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                    showProgress(false);
                                }
                            });
                } else {
                    Snackbar snackbar = Snackbar
                            .make(mParentLayout, "No Internet Connection", Snackbar.LENGTH_LONG)
                            .setAction("Try Again", view -> attemptLogin());
                    snackbar.show();
                }
            } catch (Exception e) {
                Log.e(getString(R.string.TAG), "Error: " + e.getMessage());
                showProgress(false);
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

    }
}
