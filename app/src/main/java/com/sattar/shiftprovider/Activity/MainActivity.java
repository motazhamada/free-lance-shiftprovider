package com.sattar.shiftprovider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.Classes.User;
import com.sattar.shiftprovider.Fragment.MainFragment;
import com.sattar.shiftprovider.Fragment.ProfileFragment;
import com.sattar.shiftprovider.Fragment.SettingsFragment;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.SavePrefs;
import com.sattar.shiftprovider.utilities.UserData;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    // tags used to attach the fragments
    private static final String TAG_MAIN = "my_jobs";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_SETTINGS = "settings";
    public static int navItemIndex = 0;
    public static String CURRENT_TAG = TAG_MAIN;
    private ProgressBar navigation_Progress;
    private String[] activityTitles;
    private Handler mHandler;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private UserData userData;
    private User user;
    private SavePrefs savePrefs;
    private JobItem mJobItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userData = new UserData();
        userData.isLogout(this);
        user = userData.Get_UserData(this);
        //noinspection unchecked
        savePrefs = new SavePrefs(this, JobItem.class);
        mJobItem = (JobItem) savePrefs.load();
        if (mJobItem != null && !mJobItem.getOrder_status_local().equals("not_started")) {
            Intent intent = new Intent(MainActivity.this, OrderActivity.class);
            startActivity(intent);
            finish();
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mHandler = new Handler();
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        // Navigation view header
        View navHeader = navigationView.getHeaderView(0);
        TextView txtName = navHeader.findViewById(R.id.header_name);
        TextView txtEmail = navHeader.findViewById(R.id.header_email);
        navigation_Progress = findViewById(R.id.navigation_Progress);
        //ImageView imgProfile = navHeader.findViewById(R.id.header_imageprofile);


        txtName.setText(user.getName());
        txtEmail.setText(user.getUsername());

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_MAIN;
            loadHomeFragment();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }
        if (navItemIndex != 0) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_MAIN;
            loadHomeFragment();
            return;
        }
        super.onBackPressed();
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = () -> {
            // update the cancel_order content by replacing fragments
            Fragment fragment = getHomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.container, fragment, CURRENT_TAG);
            fragmentTransaction.commitAllowingStateLoss();
        };

        // If mPendingRunnable is not null, then add to the message queue
        mHandler.post(mPendingRunnable);


        //Closing drawer on item click
        drawer.closeDrawer(GravityCompat.START);
        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // cancel_order
                return new MainFragment();
            case 1:
                // profile
                return new ProfileFragment();
            case 2:
                // settings fragment
                return new SettingsFragment();
            default:
                return new MainFragment();
        }
    }

    private void setToolbarTitle() {
        Objects.requireNonNull(getSupportActionBar()).setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {

        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        // This method will trigger on item Click of navigation menu
        navigationView.setNavigationItemSelectedListener(item -> {
            // Handle navigation view item clicks here.
            int id = item.getItemId();

            if (id == R.id.nav_logout) {
                logout();
            } else {
                if (id == R.id.nav_my_jobs) {
                    navItemIndex = 0;
                    CURRENT_TAG = TAG_MAIN;
                } else if (id == R.id.nav_profile) {
                    navItemIndex = 1;
                    CURRENT_TAG = TAG_PROFILE;
                } else if (id == R.id.nav_settings) {
                    navItemIndex = 2;
                    CURRENT_TAG = TAG_SETTINGS;
                }
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                loadHomeFragment();
            }  //Checking if the item is in checked state or not, if not make it in checked state
            if (item.isChecked()) {
                item.setChecked(false);
            } else {
                item.setChecked(true);
            }
            item.setChecked(true);

            return true;
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void logout() {
        try {
            navigation_Progress.setVisibility(View.VISIBLE);
            userData.Clear_UserData(this);
            userData.isLogout(this);
            navigation_Progress.setVisibility(View.GONE);
        } catch (Exception e) {
            navigation_Progress.setVisibility(View.GONE);
            Log.e(getString(R.string.TAG), " Exception = " + e);
        }

    }

}
