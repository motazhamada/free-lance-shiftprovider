package com.sattar.shiftprovider.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pkmmte.view.CircularImageView;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.SavePrefs;
import com.sattar.shiftprovider.utilities.UserData;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FinishedOrderActivity extends AppCompatActivity {

    Button payed_button_fo, refuse_button_fo, rate_button_fo;
    CircularImageView client_item_imageView_fo;
    RatingBar ratingBar_fo;
    TextView order_price_fo, clientName_item_textView_fo, clientPhone_item_textView_fo, clientServiceType_item_textView_fo, clientTime_item_textView_fo, feedback_editText_fo;

    ProgressBar progressBar;
    private UserData userData;
    private SavePrefs savePrefs;
    private JobItem mJobItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finished_order);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        userData = new UserData();
        userData.isLogout(this);

        //noinspection unchecked
        savePrefs = new SavePrefs(this, JobItem.class);
        mJobItem = (JobItem) savePrefs.load();

        payed_button_fo = findViewById(R.id.payed_button_fo);
        refuse_button_fo = findViewById(R.id.refuse_button_fo);
        rate_button_fo = findViewById(R.id.rate_button_fo);

        ratingBar_fo = findViewById(R.id.ratingBar_fo);
//        ratingBar_fo.setMax(5);
//        ratingBar_fo.setNumStars(5);
//        ratingBar_fo.setStepSize(1);

        client_item_imageView_fo = findViewById(R.id.client_item_imageView_fo);

        order_price_fo = findViewById(R.id.order_price_fo);
        clientName_item_textView_fo = findViewById(R.id.clientName_item_textView_fo);
        clientPhone_item_textView_fo = findViewById(R.id.clientPhone_item_textView_fo);
        clientServiceType_item_textView_fo = findViewById(R.id.clientServiceType_item_textView_fo);
        clientTime_item_textView_fo = findViewById(R.id.clientTime_item_textView_fo);
        feedback_editText_fo = findViewById(R.id.feedback_editText_fo);

        progressBar = findViewById(R.id.progressBar_fo);
        AlertDialog.Builder builder = new AlertDialog.Builder(FinishedOrderActivity.this);

        loadData();

        payed_button_fo.setOnClickListener(v -> {
            builder.setMessage("Did the customer pay ?")
                    .setTitle("Customer Payed");
            builder.setPositiveButton("Yes", (dialog, id) -> {
                showRate(true);
                dialog.dismiss();
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        refuse_button_fo.setOnClickListener(v -> {
            builder.setMessage("Did the customer Refuse To Pay ?")
                    .setTitle("Refused To Pay");
            builder.setPositiveButton("Yes", (dialog, id) -> {
//                showRate(true);
                refuseToPay();
                dialog.dismiss();
            });
            builder.setNegativeButton("Cancel", (dialog, id) -> {
                dialog.cancel();
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
        rate_button_fo.setOnClickListener(v -> {
            if (TextUtils.isEmpty(feedback_editText_fo.getText())) {
                feedback_editText_fo.setError(getString(R.string.error_field_required));
            } else {
                rateClient();
            }
        });
    }

    void rateClient() {
        // data to populate the RecyclerView with
        try {
            showPB(true);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Make_User_Rate_Address),
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                if (Objects.equals(jsonObject.getBoolean("status"), true)) {
                                    IntentToMainActivity();
                                }
                                showPB(false);
                            } catch (Exception error) {
                                showPB(false);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            showPB(false);
                            Log.e(getString(R.string.TAG), "FinishedOrderActivity 2a : ");
                        }
                    }, error -> Log.e(getString(R.string.TAG), "FinishedOrderActivity 1a : ")) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("client_id", mJobItem.getClient_id());
                    params.put("driver_id", userData.Get_UserID(FinishedOrderActivity.this));
                    params.put("rate", String.valueOf(ratingBar_fo.getRating()));
                    params.put("comment", feedback_editText_fo.getText().toString().trim());
                    params.put("lang", getString(R.string.lang));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FinishedOrderActivity 3a : " + e.toString());
        }
    }

    private void IntentToMainActivity() {
        Intent intent = new Intent(FinishedOrderActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        savePrefs.clear();
    }

    void refuseToPay() {
        // data to populate the RecyclerView with
        try {
            showPB(true);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Make_Cancel_Order_Address),
                    response -> {
                        if (response != null && !Objects.equals(String.valueOf(response), "[]")) {
                            try {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                if (Objects.equals(jsonObject.getBoolean("status"), true)) {
                                    IntentToMainActivity();
                                }
                                showPB(false);
                            } catch (Exception error) {
                                showPB(false);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            showPB(false);
                            Log.e(getString(R.string.TAG), "FinishedOrderActivity 2b : ");
                        }
                    }, error -> Log.e(getString(R.string.TAG), "FinishedOrderActivity 1b : ")) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("order_id", mJobItem.getId());
                    params.put("reason", "4");//refused to pay
                    params.put("comment", "refused to pay");
                    params.put("lang", getString(R.string.lang));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FinishedOrderActivity 3b : " + e.toString());
        }
    }

    void showPB(boolean status) {
        if (status) {
            progressBar.setVisibility(View.VISIBLE);
            rate_button_fo.setVisibility(View.GONE);
            ratingBar_fo.setVisibility(View.GONE);

        } else {
            progressBar.setVisibility(View.GONE);
            rate_button_fo.setVisibility(View.VISIBLE);
            ratingBar_fo.setVisibility(View.VISIBLE);
        }
    }

    void showRate(boolean status) {
        if (status) {
            payed_button_fo.setVisibility(View.GONE);
            refuse_button_fo.setVisibility(View.GONE);
            rate_button_fo.setVisibility(View.VISIBLE);
            ratingBar_fo.setVisibility(View.VISIBLE);
            feedback_editText_fo.setVisibility(View.VISIBLE);
        } else {
            payed_button_fo.setVisibility(View.VISIBLE);
            refuse_button_fo.setVisibility(View.VISIBLE);
            rate_button_fo.setVisibility(View.GONE);
            ratingBar_fo.setVisibility(View.GONE);
            feedback_editText_fo.setVisibility(View.GONE);
        }
    }

    private void loadData() {
        order_price_fo.setText(String.format("%s SAR", mJobItem.getPrice()));
        clientName_item_textView_fo.setText(mJobItem.getClient_name());
        clientPhone_item_textView_fo.setText(mJobItem.getClient_mobile());
        clientServiceType_item_textView_fo.setText(String.format("Service : %s", mJobItem.getService_name()));
        clientTime_item_textView_fo.setText(String.format("Time : %s", mJobItem.getTime_from()));
    }
}
