package com.sattar.shiftprovider.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pkmmte.view.CircularImageView;
import com.sattar.shiftprovider.Activity.OrderActivity;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.SavePrefs;

import java.util.List;

public class JobRecyclerViewAdapter extends RecyclerView.Adapter<JobRecyclerViewAdapter.ViewHolder> implements
        android.view.View.OnClickListener {

    //    private AlertDialog alertDialog = null;
    private List<JobItem> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private View mView;
    private String mType;
    private SavePrefs savePrefs;

    // data is passed into the constructor
    public JobRecyclerViewAdapter(Context context, List<JobItem> data, View view, String type) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mView = view;
        this.mType = type;
        this.savePrefs = new SavePrefs(mContext,JobItem.class);
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    public void add(int position, JobItem item) {
        mData.add(position, item);
        notifyItemInserted(position);
    }

    public void animateTo(List<JobItem> item) {
        applyAndAnimateRemovals(item);
        applyAndAnimateAdditions(item);
        applyAndAnimateMovedItems(item);
    }

    private void applyAndAnimateRemovals(List<JobItem> newModels) {
        for (int i = mData.size() - 1; i >= 0; i--) {
            final JobItem model = mData.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<JobItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final JobItem model = newModels.get(i);
            if (!mData.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<JobItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final JobItem model = newModels.get(toPosition);
            final int fromPosition = mData.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public JobItem removeItem(int position) {
        final JobItem model = mData.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, JobItem model) {
        mData.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final JobItem model = mData.remove(fromPosition);
        mData.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void remove(JobItem item) {
        int position = mData.indexOf(item);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button jobStart_item_button;
        CircularImageView client_item_imageView;
        TextView clientName_item_textView, clientPhone_item_textView, clientServiceType_item_textView, clientTime_item_textView;
        View jobStart_item_splitter;
        ViewHolder(View itemView) {
            super(itemView);
            jobStart_item_splitter = itemView.findViewById(R.id.jobStart_item_splitter);
            jobStart_item_button = itemView.findViewById(R.id.jobStart_item_button);
            client_item_imageView = itemView.findViewById(R.id.client_item_imageView);
            clientName_item_textView = itemView.findViewById(R.id.clientName_item_textView);
            clientPhone_item_textView = itemView.findViewById(R.id.clientPhone_item_textView);
            clientServiceType_item_textView = itemView.findViewById(R.id.clientServiceType_item_textView);
            clientTime_item_textView = itemView.findViewById(R.id.clientTime_item_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // binds the data to the TextView in each row
    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        JobItem JobItem = mData.get(position);
        try {

            holder.clientName_item_textView.setText(JobItem.getClient_name());
            holder.clientPhone_item_textView.setText(JobItem.getClient_mobile());
            holder.clientServiceType_item_textView.setText(String.format("Service : %s", JobItem.getService_name()));
            holder.clientTime_item_textView.setText(String.format("Time : %s", JobItem.getTime_from()));
            if (mType.equals("Finished") || mType.equals("Canceled")) {
                holder.jobStart_item_button.setVisibility(View.GONE);
                holder.jobStart_item_splitter.setVisibility(View.VISIBLE);
//            holder.jobStart_item_button.setText("");
            } else {
                holder.jobStart_item_button.setOnClickListener(v -> {
                    Intent intent = new Intent(mContext, OrderActivity.class);
                    JobItem.setOrder_status_local("not_started");
                    savePrefs.save(JobItem);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("Order_ID",JobItem.getId());
//                    bundle.putString("Client_ID",JobItem.getClient_id());
//                    bundle.putString("Client_Name",JobItem.getClient_name());
//                    bundle.putString("Client_Mobile",JobItem.getClient_mobile());
//                    bundle.putString("Service_Name",JobItem.getService_name());
//                    bundle.putString("Time",JobItem.getTime_from());
//                    bundle.putString("Order_Price",JobItem.getPrice());
//                    bundle.putString("Client_Latitude",JobItem.getClient_latitude());
//                    bundle.putString("Client_Longitude",JobItem.getClient_longitude());
//                    intent.putExtra("Order_Info",bundle);
                    mContext.startActivity(intent);
                });
            }

        } catch (Exception e) {
            Log.e(mContext.getString(R.string.TAG), " JobRecyclerViewAdapter 1 = " + e);
        }
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // convenience method for getting data at click position
    public JobItem getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

    }


    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


}