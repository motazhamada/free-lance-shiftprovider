package com.sattar.shiftprovider.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sattar.shiftprovider.Adapter.JobRecyclerViewAdapter;
import com.sattar.shiftprovider.Classes.JobItem;
import com.sattar.shiftprovider.R;
import com.sattar.shiftprovider.utilities.UserData;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class FinishedJobFragment extends Fragment {

    private static List<JobItem> jobItem, jobItemList;

    private ProgressBar progressBar;
    private JobRecyclerViewAdapter adapter;

    private UserData userData;

    public FinishedJobFragment() {
    }

    @SuppressWarnings("unused")
    public static FinishedJobFragment newInstance() {
        FinishedJobFragment fragment = new FinishedJobFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        try {
            userData = new UserData();
            jobItem = new ArrayList<>();
            ConstraintLayout notFinished_ConstrainLayout = view.findViewById(R.id.job_constrainLayout);
            progressBar = view.findViewById(R.id.job_progressBar);
            RecyclerView recyclerView = view.findViewById(R.id.job_recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//            loadFinishedJob();
            // set up the RecyclerView
            adapter = new JobRecyclerViewAdapter(getContext(), jobItem, notFinished_ConstrainLayout, "Finished");
            // adapter.setClickListener(this);
            recyclerView.setAdapter(adapter);

        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FinishedJobFragment 1: " + e.toString());
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        loadFinishedJob();
    }

    void loadFinishedJob() {
        // data to populate the RecyclerView with
        try {
            jobItem.clear();
            progressBar.setVisibility(View.VISIBLE);
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(Objects.requireNonNull(getContext()));
            StringRequest request = new StringRequest(Request.Method.POST,
                    getString(R.string.Resource_Address) + getString(R.string.Get_Driver_Finished_Orders_Address),
                    response -> {
                        Log.d(getString(R.string.TAG), "response : " + response);
                        if (String.valueOf(response) != null && !Objects.equals(response, "[]")) {
                            try {
                                JSONObject jsonObject = new JSONObject(String.valueOf(response));
                                Gson gson = new Gson();
                                jobItemList = gson.fromJson(String.valueOf(jsonObject.getString("data")), new TypeToken<ArrayList<JobItem>>() {
                                }.getType());
                                jobItem.addAll(jobItemList);
                                adapter.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                            } catch (Exception error) {
                                Log.d(getString(R.string.TAG), "5");
                                progressBar.setVisibility(View.GONE);
                                //If an error occurs that means end of the list has reached
                            }
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Log.e(getString(R.string.TAG), "FinishedJobFragment 2");
                        }
                    }, error -> {

            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("driver_id", userData.Get_UserID(getActivity()));
                    params.put("lang", getString(R.string.lang));
                    return params;
                }
            };
            // Adding request to request queue
            queue.add(request);
        } catch (Exception e) {
            Log.e(getString(R.string.TAG), "FinishedJobFragment 3: " + e.toString());
        }
    }

}
