package com.sattar.shiftprovider.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.sattar.shiftprovider.Activity.LoginActivity;
import com.sattar.shiftprovider.Classes.User;

import static android.content.Context.MODE_PRIVATE;

public class UserData {

    public void Set_UserID(Context activity, String UserID) {
        SharedPreferences.Editor sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE).edit();
        sharedPref.putString("UserID", UserID);
        sharedPref.apply();
        sharedPref.commit();
    }

    public void Set_UserData(Context activity, User user) {
        SharedPreferences.Editor sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE).edit();
        String UserUser = new Gson().toJson(user);
        sharedPref.putString("UserUser", UserUser);
        sharedPref.apply();
        sharedPref.commit();
    }


    public boolean Check_UserID(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("UserID", null);
        return restoredText != null;
    }


    public String Get_UserID(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("UserID", null);
        if (restoredText != null) {
            return restoredText;
        }
        return "No User Token";
    }

    public User Get_UserData(Context activity) {
        SharedPreferences sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE);
        String restoredText = sharedPref.getString("UserUser", "");
        return (User) new Gson().fromJson(restoredText, User.class);
    }


    public void Clear_UserData(Context activity) {
        SharedPreferences.Editor sharedPref = activity.getSharedPreferences("UserData", MODE_PRIVATE).edit();
        sharedPref.clear();
        sharedPref.apply();
    }

    public void isLogout(Activity activity) {
        if (!Check_UserID(activity)) {
            Clear_UserData(activity);
            Intent intent = new Intent(activity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }
}
